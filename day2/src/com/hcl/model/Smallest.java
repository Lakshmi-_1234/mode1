package com.hcl.model;

public class Smallest {
	public static int relational(int a, int b, int c) {
		int min;
		if (a < b && a < c) {
			min = a;
		} else if (b < c && b < a) {
			min = b;

		} else {
			min = c;
		}

		return min;

	}

}
