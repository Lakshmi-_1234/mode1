package com.hcl.model;

public class Room {

	private int roomNo;
	private String roomArea;
	private String acMachine;
	private String roomType;

	public Room(int roomNo, String roomArea, String acMachine, String roomType) {
		super();
		this.roomNo = roomNo;
		this.roomArea = roomArea;
		this.acMachine = acMachine;
		this.roomType = roomType;
	}

	public Room() {
		super();
	}

	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public String getRoomArea() {
		return roomArea;
	}

	public void setRoomArea(String roomArea) {
		this.roomArea = roomArea;
	}

	public String getAcMachine() {
		return acMachine;
	}

	public void setAcMachine(String acMachine) {
		this.acMachine = acMachine;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

}
