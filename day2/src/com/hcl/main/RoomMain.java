package com.hcl.main;

import com.hcl.model.Room;

public class RoomMain {

	public static void main(String[] args) {

		Room room = new Room();
		room.setRoomNo(15 - 2);
		room.setRoomArea("Madanapalle");
		room.setAcMachine("Whirlpool");
		room.setRoomType("Guestroom");
		System.out.println("Room number is:" + room.getRoomNo());
		System.out.println("Room area:" + room.getRoomArea());
		System.out.println("AC Machine:" + room.getAcMachine());
		System.out.println("Room type:" + room.getRoomType());

	}

}
