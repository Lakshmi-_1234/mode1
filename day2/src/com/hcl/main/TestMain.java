package com.hcl.main;

import com.hcl.model.Child1;
import com.hcl.model.Child2;
import com.hcl.model.Parent;

public class TestMain {
	public static void main(String[] args)

	{

		Parent parent = new Parent();
		Child1 c1 = new Child1();

		Child2 c2 = new Child2();

		System.out.println("c1 instanceof Parent");

		System.out.println("c2 instanceof Parent");

		System.out.println("p instanceof Child1");

		System.out.println("p instanceof Child2");

		parent = c1;

		System.out.println("p instanceof Child1");

		System.out.println("p instanceof Child2");

		parent = c2;

		System.out.println("p instanceof Child1");

		System.out.println("p instanceof Child2");

	}

}
