package com.hcl.main;

import com.hcl.model.MiddleString;

import java.util.Scanner;

public class MiddleStringMain {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		MiddleString middlestring = new MiddleString();
		System.out.println("input string:");
		String str = in.nextLine();
		System.out.println("middle character in the string:"+middlestring.MiddleCharacter(str));

		in.close();

	}

}
