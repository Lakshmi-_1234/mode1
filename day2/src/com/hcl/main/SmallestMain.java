package com.hcl.main;

import com.hcl.model.Smallest;

import java.util.Scanner;

public class SmallestMain {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int r = Smallest.relational(45, 105, 100);

		System.out.println("The smallest number is " + r);
		scanner.close();

	}

}
