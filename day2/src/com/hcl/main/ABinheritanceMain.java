package com.hcl.main;

import com.hcl.model.A;
import com.hcl.model.B;

public class ABinheritanceMain {

	public static void main(String[] args) {
		A a = new A();
		B b = new B();
		a.setEmpName("Lakshmi");
		a.setEmpNo(1234);

		System.out.println(a.getEmpNo());
		System.out.println(a.getEmpName());
		b.setSalary(456.6f);
		System.out.println(b.getSalary());

	}

}
