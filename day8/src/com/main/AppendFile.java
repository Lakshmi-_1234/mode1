package com.main;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

public class AppendFile {

	public static void main(String[] args) throws Exception {
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter pw = null;

		try {
			fw = new FileWriter("D:\\9th-29th\\lucky.txt", true);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

			pw.println("Welcome");
			pw.println("To");
			pw.println("JAVA PROGRAMMING");
			System.out.println("Data Successfully appended into file");
			pw.flush();
		} finally {

			pw.close();
			bw.close();
			fw.close();
		}
	}

}
