::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
PRE-REQUIREMENTS
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::;

CUSTOMER TABLE CREATION:
_________________

mysql> create table Customer_tab(customer_num int primary key auto_increment,customer_FN varchar(15) not null,customer_LN varchar(15) not null,location varchar(15) not null);
Query OK, 0 rows affected (0.05 sec)

mysql> desc customer_tab;
+--------------+-------------+------+-----+---------+----------------+
| Field        | Type        | Null | Key | Default | Extra          |
+--------------+-------------+------+-----+---------+----------------+
| customer_num | int         | NO   | PRI | NULL    | auto_increment |
| customer_FN  | varchar(15) | NO   |     | NULL    |                |
| customer_LN  | varchar(15) | NO   |     | NULL    |                |
| location     | varchar(15) | NO   |     | NULL    |                |
+--------------+-------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)

---------------------------------------------

INSERTING RECORDS INTO THE TABLE 
---------------------------------

mysql> insert into customer_tab`(customer_FN,customer_LN,location) values('lakshmi','prasanna','chennai');
Query OK, 1 row affected (0.01 sec)

mysql> insert into customer_tab(customer_FN,customer_LN,location) values('vyshnavi','pottekula','banglore');
Query OK, 1 row affected (0.01 sec)

mysql> insert into customer_tab(customer_FN,customer_LN,location) values('leela','duthala','Hyderabad');
Query OK, 1 row affected (0.01 sec)
-------------------------------------------------------
RECORDS IN THE CUSTOMER_TAB TABLE:
:::::::::::::::::::::::::::::::::::

mysql> select * from customer_tab;
+--------------+-------------+-------------+-----------+
| customer_num | customer_FN | customer_LN | location  |
+--------------+-------------+-------------+-----------+
|            1 | lakshmi     | prasanna    | chennai   |
|            2 | vyshnavi    | pottekula   | banglore  |
|            3 | leela       | duthala     | Hyderabad |
+--------------+-------------+-------------+-----------+
3 rows in set (0.00 sec)

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::;;

ACCOUNT_TAB TABLE CREATION :
-----------------------------------------------
mysql> create table account_tab(account_num int primary key auto_increment,account_OD date not null,customer_num int,foreign key(customer_num) references customer_tab(customer_num) );
Query OK, 0 rows affected (0.45 sec)
-----------------------------------------------------------------

INSERTING RECORDS INTO ACCOUNT_TAB;

mysql> insert into account_tab values(98789687,'2019-03-22',1);
Query OK, 1 row affected (0.01 sec)

mysql> insert into account_tab values(34567245,'2018-12-31',3);
Query OK, 1 row affected (0.01 sec)

mysql> insert into account_tab values(93562736,'2001-12-26',1);
Query OK, 1 row affected (0.01 sec)

mysql> insert into account_tab values(913245672,'2009-05-21',2);
Query OK, 1 row affected (0.01 sec)

mysql> insert into account_tab values(956735627,'1998-04-22',1);
Query OK, 1 row affected (0.01 sec)

-------------------------------------------------------------------------

RECORDS IN THE ACCOUNT_TAB 
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

mysql> select * from account_tab;
+-------------+------------+--------------+
| account_num | account_OD | customer_num |
+-------------+------------+--------------+
|    34567245 | 2018-12-31 |            3 |
|    93562736 | 2001-12-26 |            1 |
|    98789687 | 2019-03-22 |            1 |
|   913245672 | 2009-05-21 |            2 |
|   956735627 | 1998-04-22 |            1 |
+-------------+------------+--------------+
5 rows in set (0.00 sec)

-----------------------------------------------------------------------
LOANDETAILS TABLE CREATION
-----------------------------------------------------------------------

mysql> create table loanDetails(loanid int primary key auto_increment,customer_num int,foreign key(customer_num) references customer_tab(customer_num));
Query OK, 0 rows affected (0.16 sec)

mysql> desc loanDetails;
+--------------+------+------+-----+---------+----------------+
| Field        | Type | Null | Key | Default | Extra          |
+--------------+------+------+-----+---------+----------------+
| loanid       | int  | NO   | PRI | NULL    | auto_increment |
| customer_num | int  | YES  | MUL | NULL    |                |
+--------------+------+------+-----+---------+----------------+
2 rows in set (0.01 sec)

-------------------------------------------------------------------
INSERTING RECORDS
-------------------------------------------------------------------
mysql> insert into loanDetails values(1234,2);
Query OK, 1 row affected (0.01 sec)

mysql> insert into loanDetails values(3452,1);
Query OK, 1 row affected (0.01 sec)

mysql> insert into loanDetails values(2345,2);
Query OK, 1 row affected (0.01 sec)

-------------------------------------------------------------------

Extracting the records in the table
---------------------------------------------------------------------

mysql> select * from loanDetails;
+--------+--------------+
| loanid | customer_num |
+--------+--------------+
|   3452 |            1 |
|   1234 |            2 |
|   2345 |            2 |
+--------+--------------+
3 rows in set (0.00 sec)


:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

1.Write a query to display account number, customer’s number, customer’s firstname,lastname,account opening date.create table 
Display the records sorted in ascending order based on account number.

SELECT account_number,am.customer_number,firstname,lastname,account_opening_date
FROM customer_master cm INNER JOIN account_master am
ON cm.customer_number=am.customer_number
ORDER BY account_number
----------------------------------------
ANSWER
----------------------------------------

mysql> SELECT account_num,at.customer_num,customer_FN,customer_LN,account_OD
    -> from customer_tab ct inner join account_tab at
    -> on ct.customer_num = at.customer_num
    -> order by account_num;
+-------------+--------------+-------------+-------------+------------+
| account_num | customer_num | customer_FN | customer_LN | account_OD |
+-------------+--------------+-------------+-------------+------------+
|    34567245 |            3 | leela       | duthala     | 2018-12-31 |
|    93562736 |            1 | lakshmi     | prasanna    | 2001-12-26 |
|    98789687 |            1 | lakshmi     | prasanna    | 2019-03-22 |
|   913245672 |            2 | vyshnavi    | pottekula   | 2009-05-21 |
|   956735627 |            1 | lakshmi     | prasanna    | 1998-04-22 |
+-------------+--------------+-------------+-------------+------------+
5 rows in set (0.00 sec)



::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

2.Write a query to display the number of customer’s from Chennai. Give the count an alias name of Cust_Count.
SELECT count(customer_number) Cust_Count
FROM customer_master
WHERE customer_city='Chennai'
----------------------------------------------------------------------
ANSWER
----------------------------------------------------------------------

mysql> SELECT count(customer_num) Cust_Count
    -> from customer_tab
    -> where location='chennai';
+------------+
| Cust_Count |
+------------+
|          1 |
+------------+
1 row in set (0.00 sec)

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::



3.Write a query to display the customer number, customer firstname,account number for the customer’s whose accounts were created after 15th of any month.
Display the records sorted in ascending order based on customer number and then by account number.

SELECT am.customer_number, firstname, account_number
FROM customer_master cm INNER JOIN account_master am
ON cm.customer_number=am.customer_number
WHERE extract(day from account_opening_date)>15
ORDER BY am.customer_number, account_number
---------------------------------------------------------------
ANSWER
----------------------------------------------------------------
mysql> SELECT at.customer_num,customer_FN, account_num
    -> FROM customer_tab ct INNER JOIN account_tab at
    -> ON ct.customer_num=at.customer_num
    -> WHERE extract(day from account_OD)>15
    -> ORDER BY at.customer_num, account_num;
+--------------+-------------+-------------+
| customer_num | customer_FN | account_num |
+--------------+-------------+-------------+
|            1 | lakshmi     |    93562736 |
|            1 | lakshmi     |    98789687 |
|            1 | lakshmi     |   956735627 |
|            2 | vyshnavi    |   913245672 |
|            3 | leela       |    34567245 |
+--------------+-------------+-------------+
5 rows in set (0.00 sec)
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

4.Write a query to display the number of customers who have registration but no account in the bank.
Give the alias name as Count_Customer for number of customers.
SELECT count(customer_number) Count_Customer
FROM customer_master
WHERE customer_number NOT IN (SELECT customer_number FROM account_master)
-------------------------------------------------------------------------------
ANSWER
-------------------------------------------------------------------------------
mysql> SELECT count(customer_num) Count_Customer
    -> FROM customer_tab
    -> WHERE customer_num NOT IN (SELECT customer_num FROM account_tab);
+----------------+
| Count_Customer |
+----------------+
|              0 |
+----------------+
1 row in set (0.00 sec)
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

5.Write a query to display the firstname of the customers who have more than 1 account. Display the records in sorted order based on firstname.
select firstname
FROM customer_master cm INNER JOIN account_master am ON cm.customer_number=am.customer_number group by firstname having count(account_number)>1 order by firstname;
-------------------------------------------------------------------------------
ANSWER
-------------------------------------------------------------------------------
mysql> select customer_FN
    -> FROM customer_tab ct INNER JOIN account_tab at ON ct.customer_num=at.customer_num group by customer_FN having count(account_num)>1 order by customer_FN;
+-------------+
| customer_FN |
+-------------+
| lakshmi     |
+-------------+
1 row in set (0.01 sec)

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

6.Write a query to display the number of clients who have asked for loans but they don’t have any account in the bank though they are registered customers. Give the count an alias name of Count.
SELECT count(ld.customer_number) Count
FROM customer_master cm INNER JOIN loan_detailsld
ON cm.customer_number=ld.customer_number
WHERE cm.customer_number NOT IN ( SELECTcustomer_number FROM account_master)
-------------------------------------------------------------------------------
ANSWERS
-----------------------------------------------------------------------------
mysql> SELECT count(ld.customer_num) Count
    -> FROM customer_tab ct INNER JOIN loanDetails ld
    -> ON ct.customer_num=ld.customer_num
    -> WHERE ct.customer_num NOT IN ( SELECT customer_num FROM account_tab)
    -> ;
+-------+
| Count |
+-------+
|     0 |
+-------+
1 row in set (0.00 sec)
------------------------------------------------------------------------------


7.Write a query to display the customer’s firstname who have multiple accounts (atleast 2 accounts). Display the records sorted in ascending order based on customer's firstname.
SELECT firstname
FROM customer_master INNER JOIN account_master
ON customer_master.customer_number=account_master.customer_number GROUP BY firstname
having count(firstname)>=2 order by firstname;

-------------------------------------------------------------------------------
ANSWER
-------------------------------------------------------------------------------
mysql> SELECT customer_FN
    -> FROM customer_tab INNER JOIN account_tab
    -> ON customer_tab.customer_num=account_tab.customer_num GROUP BY customer_FN
    -> having count(customer_FN)>=2 order by customer_FN;
+-------------+
| customer_FN |
+-------------+
| lakshmi     |
+-------------+
1 row in set (0.00 sec)
-------------------------------------------------------------------------------