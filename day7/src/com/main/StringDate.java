package com.main;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringDate {

	public static void main(String[] args) {
		String pattern = "MM-dd-yyyy";
		String pattern2 = "HH:mm:ss";
		String pattern3 = "dd MMMM yyyy  zzzz";
		String pattern4 = "E,dd MMM yyyy hh:mm:ss z";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);
		SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat(pattern3);
		SimpleDateFormat simpleDateFormat4 = new SimpleDateFormat(pattern4);
		String date = simpleDateFormat.format(new Date());
		String date2 = simpleDateFormat2.format(new Date());
		String date3 = simpleDateFormat3.format(new Date());
		String date4 = simpleDateFormat4.format(new Date());

		System.out.println(date);
		System.out.println(date2);
		System.out.println(date3);
		System.out.println(date4);

	}

}
