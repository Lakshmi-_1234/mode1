package com.main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main10 {
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter first date in formate 'yyy-mm-dd'");
		String d1 = scanner.next();
		System.out.println("Enter second date in formate 'yyy-mm-dd'");
		String d2 = scanner.next();
		Date d3 = sdformat.parse(d1);
		Date d4 = sdformat.parse(d2);
		System.out.println("The date 1 is: " + sdformat.format(d3));
		System.out.println("The date 2 is: " + sdformat.format(d4));
		if (d3.compareTo(d4) > 0) {
			System.out.println("Date 1 occurs after Date 2:" + d2);
		} else if (d3.compareTo(d4) < 0) {
			System.out.println("Date 1 occurs before Date 2:" + d1);
		} else if (d3.compareTo(d4) == 0) {
			System.out.println("Both dates are equal");
		}
		scanner.close();

	}

}
