package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

import com.model.Model13;

public class Main13 {
	public static void main(String[] args) throws ParseException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the dates");
		String s1 = br.readLine();
		String s2 = br.readLine();
		System.out.println(Model13.getDateDifference(s1, s2));
	}

}
