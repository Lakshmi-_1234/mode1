package com.main;

import java.util.Scanner;

import com.model.Model12;

public class Main12 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter ip address");

		String ipAddress = sc.nextLine();

		boolean b = Model12.ipValidator(ipAddress);
		if (b == true)
			System.out.println("Valid");
		else
			System.out.println("Invalid");

		sc.close();
	}

}
