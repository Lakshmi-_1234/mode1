package com.hcl.model;

public class Palindrome {

	public String checkPalindrome(String str) {
		String reverse = "";
		int length = str.length();
		for (int i = length - 1; i >= 0; i--) {

			reverse = reverse + str.charAt(i);

		}
		return reverse;

	}

}
