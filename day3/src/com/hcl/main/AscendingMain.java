package com.hcl.main;

import com.hcl.model.Ascending;

public class AscendingMain {

	public static void main(String[] args) {

		int[] obj = new int[] { 5, 1, 9, -2, -6 };
		System.out.println("Elements of given array");

		Ascending.printArray(obj);
		Ascending.sortArray(obj);

	}

}
