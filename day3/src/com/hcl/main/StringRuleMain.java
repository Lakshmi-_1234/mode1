package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.StringRule;

public class StringRuleMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		StringRule stringrule = new StringRule();
		System.out.println("Enter the string:");
		String temp = scanner.next();
		System.out.println("string after checking string rule:" + stringrule.remove(temp));
		scanner.close();

	}

}
