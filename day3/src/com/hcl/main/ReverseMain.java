package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.ReverseString;

public class ReverseMain {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		ReverseString reverseString = new ReverseString();
		System.out.println("enter the string:");
		String str = scanner.next();
		System.out.println("Reversed string is:" + reverseString.wordReverse(str));
		scanner.close();

	}

}
