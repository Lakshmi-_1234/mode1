package com.hcl.main;

import com.hcl.model.Palindrome;

public class PalindromeMain {

	public static void main(String[] args) {

		Palindrome palindrome = new Palindrome();
		String str = "amma";

		String temp = palindrome.checkPalindrome(str);

		System.out.println("original string:" + str);

		System.out.println("Reversed string: " + temp);
		if (temp.equals(str)) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}
	}

}
