package com.hcl.main;

public class LowerCase {

	public static void main(String[] args) {
		String str = "WELCOME TO  HCL TECHNOLOGIES";
		String lowerstr = str.toLowerCase();
		System.out.println("Given string in lowercase:" + lowerstr);

	}

}
