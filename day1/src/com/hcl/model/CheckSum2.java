package com.hcl.model;

public class CheckSum2 {
	public static int Even(int n1) {

		int sum = 0, n2;
		while (n1 > 0) {
			n2 = n1 % 10;

			if (n2 % 2 == 0) {

				sum = sum + n2 * n2;

			}
			n1 = n1 / 10;

		}

		return sum;

	}

}
