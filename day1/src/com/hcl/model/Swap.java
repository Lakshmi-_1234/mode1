package com.hcl.model;

public class Swap {
	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		int temp;
		System.out.println("before swapping:" + a + "\n" + b);
		temp = a;
		a = b;
		b = temp;
		System.out.println("after swapping:" + a + "\n" + b);

	}
}
