package com.hcl.model;

public class CheckSum {
	public int Odd(int n1) {

		int sum = 0, r, n2;
		while (n1 > 0) {
			n2 = n1 % 10;

			if (n2 % 2 != 0) {

				sum = sum + n2;
			}
			n1 = n1 / 10;

		}

		if (sum % 2 == 0) {

			r = 1;

		} else {
			r = -1;

		}
		return r;

	}

}
