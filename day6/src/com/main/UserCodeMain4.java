package com.main;

public class UserCodeMain4 extends Thread  {

	public static void main(String[] args) {
		DemoThread obj1 = new DemoThread();
		DemoThread obj2 = new DemoThread();
		DemoThread obj3 = new DemoThread();
		obj1.run();

	}

	public UserCodeMain4() {

		UserCodeMain1 t1 = new UserCodeMain1();

		System.out.println("Name of t1:" + t1.getName());

		t1.start();

	}

	@Override
	public void run() {
		try {
		System.out.println("running child Thread in loop:");
		int counter = 1;
		
		while (counter < 11) {
			System.out.println(counter);
			counter = counter + 1;
			
				Thread.sleep(2000);
			} }catch (InterruptedException e) {

				e.printStackTrace();
			}

	}

}
