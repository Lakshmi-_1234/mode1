package com.main;

public class DemoThread implements Runnable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DemoThread obj1 = new DemoThread();
		DemoThread obj2= new DemoThread();
		DemoThread obj3= new DemoThread();
		obj1.run();
		

	}

	public DemoThread() {

		UserCodeMain1 t1 = new UserCodeMain1();

		System.out.println("Name of t1:" + t1.getName());

		t1.start();

	}

	@Override
	public void run() {
		System.out.println("running child Thread in loop:");
		int counter = 1;
		while (counter < 11) {
			System.out.println(counter);
		counter = counter + 1;
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		}

	}

}
