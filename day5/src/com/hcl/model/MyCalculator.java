package com.hcl.model;

public class MyCalculator {
	int number;
	int power;
	public MyCalculator() {
		super();
	}
	public  MyCalculator(int number, int power) {
		super();
		this.number = number;
		this.power = power;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	
}




