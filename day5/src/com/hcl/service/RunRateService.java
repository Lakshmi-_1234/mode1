package com.hcl.service;

import com.hcl.model.RunRate;

public class RunRateService extends RunRate {
	public int runRate(int runs, int overs) {
		return runs / overs;
	}

}
