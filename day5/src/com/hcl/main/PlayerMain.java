package com.hcl.main;

import java.util.Scanner;

import com.hcl.exception.PlayerException;
import com.hcl.model.Player;

public class PlayerMain {

	public void main(String[] args) throws PlayerException {

		Scanner scanner = new Scanner(System.in);
		Player player = new Player();
		try {
			System.out.println("Enter Player Name:");
			String name = scanner.next();
			System.out.println("Enter Age:");
			int age = scanner.nextInt();
			if (age > 19) {
				System.out.println("Player Name:" + name);
				System.out.println("Player Age:" + age);
			} else {
				throw new PlayerException();
			}
		} catch (PlayerException e) {
			System.out.println(e.getMessage());
		} finally {

			player = null;
		}
		scanner.close();

	}

}
