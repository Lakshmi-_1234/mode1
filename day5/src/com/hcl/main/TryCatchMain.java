package com.hcl.main;

import java.util.Scanner;

import com.hcl.service.TryCatchService;

public class TryCatchMain {

	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(System.in);
		TryCatchService tryCatch = new TryCatchService();
		try {
			System.out.println("Enter the numbers:");
			int num1 = scanner.nextInt();
			int num2 = scanner.nextInt();
			System.out.println("Division of numbers is:" + tryCatch.TryCatchService(num1, num2));
		} catch (java.util.InputMismatchException e) {
			System.out.println(e);
		} catch (java.lang.ArithmeticException e1) {
			System.out.println(e1);
		}

		finally {

			tryCatch = null;

		}
		scanner.close();

	}
}
